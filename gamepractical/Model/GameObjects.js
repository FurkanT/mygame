﻿var GameObjects = window.GameObjects || {};
GameObjects.Character = (function () {
    return {
        sprite: "",
        bFacingRight: "",
        Name: "",
        init: function (CharName) {
            bFacingRight = true;
            Name = CharName;
            sprite = game.add.sprite(40, 345, 'buyucu_walking');
            // sprite.animations.add('walk');
            
            sprite.animations.add('walk', [36,37,38,39,40,41,42,43]);
            sprite.animations.add('stand', [34]);
            sprite.animations.add('attack1', [21,13,14,15,16,17,19,20]);
            sprite.animations.add('levelup', [29,30,31,32]);
            sprite.animations.add('attack2', [9,10,11,12]);
            sprite.animations.add('prepare', [  5, 6, 7,8,]);
            sprite.animations.add('melee',[46,45,44])
            sprite.animations.add('death', [23, 24, 25, 26, 27, 28]);
            sprite.animations.add('attack3', [0,1,2,3]);
            sprite.animations.play('stand', 8, true);
            sprite.anchor.setTo(.5, .5);

        },
        GetName: function(){
        return Name;
},
    MoveRight: function (){
        sprite.x += 2;
        if(!bFacingRight){
            sprite.scale.x *=-1;
            bFacingRight =true;

        }
        sprite.animations.play('walk', 8, true);
        console.log("right");
    },
MoveLeft: function(){
    sprite.x -= 2;
    
    if (bFacingRight){
        sprite.scale.x *=-1;
        bFacingRight=false;
    }
    sprite.animations.play('walk',8,true)
    console.log("left");
},
Jump: function () {
    sprite.animations.play('jump', 8, true);
    console.log("jump");
    
},
Stand: function () {
    sprite.animations.play('stand', 8, true);
    console.log("stand");
},
Attack1: function(){
    sprite.animations.play('attack1',8,true);
},
Attack2: function () {
    sprite.animations.play('attack2',8,true);
},
Attack3: function () {
    sprite.animations.play('attack3',8,true);
},
MeleeAttack: function () {
    sprite.animations.play('melee',8,true);
},

};
});