﻿var showLogs = true;
var keys;

if (showLogs) {
    console.log("Inside ScreenManager");
}
var skill1;
var skill2;
var skill3;
var melee;
var canAttack = true;
var gameWidth = 800;
var gameHeight = 450;
var backgroundColor = "#000000";
var game = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, 'KulturGameDev', {
    preload: preload, create: create, update: update });
var PlayerCharacter;
function preload() {
    if (showLogs) {
        console.log("ScreenManager - preload");
        
    }
    game.load.image('background2', 'Pictures/original.png');
    game.load.atlas('buyucu_walking', 'Sprites/Buyucu.png', 'Sprites/Buyucu.json',
    Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
}   
function create() {
    if (showLogs)
        console.log("ScreenManager - create");
    var Background = game.add.sprite(game.world.centerX, game.world.centerY, 'background2');
    Background.anchor.setTo(0.5, 0.5);    keys = game.input.keyboard.createCursorKeys();    PlayerCharacter = new GameObjects.Character();
    PlayerCharacter.init("buyucu");    skill1 = game.input.keyboard.addKey(Phaser.Keyboard.Z);    skill2 = game.input.keyboard.addKey(Phaser.Keyboard.X);    skill3 = game.input.keyboard.addKey(Phaser.Keyboard.C);    melee = game.input.keyboard.addKey(Phaser.Keyboard.V);    
}
function update() {
    if (keys.left.isDown) {
        PlayerCharacter.MoveLeft();
    }
    else if (keys.right.isDown) {
        PlayerCharacter.MoveRight();
    }
    else if (keys.up.isDown) {
        PlayerCharacter.Jump();}
    else { PlayerCharacter.Stand(); }
    if(melee.isDown) {
        sprite.animations.play('melee', 8, true);
    }
    if (skill1.isDown) {
        PlayerCharacter.Attack1();
    }
    if (skill2.isDown) {
        if (canAttack) {
            PlayerCharacter.Attack2();
            canAttack = false;
            game.time.events.add(300, (function () { canAttack = true; }), this);
        }
        }
    if (skill3.isDown) {
        PlayerCharacter.Attack3();
    }
    
}

    //if (keys.left.isDown) {
    //    character.x--;
    //    if (bFacingRight) {
    //        character.scale.x *= -1;
    //        bFacingRight = false;
    //    }
    //    console.log("left");
    //}
    //else if (keys.right.isDown) {
    //    character.x++;
    //    if (!bFacingRight) {
    //        character.scale.x *= -1;
    //        bFacingRight = true;
    //    }
    //    console.log("right");
    //}